#!/usr/bin/env bash
#
# Usage example:
# bash docker_network_all_subnets_cidr.sh
#
# Output example (for default Docker network in Ubuntu 18.04):
#
# 8a968dd6e883: bridge, 172.17.0.0/16
# bde82cf794a8: host,
# ba51a24e07ac: none,
#
# Copyright:
# Distributed under CC0 1.0 license or newer.
#
# Source:
# https://gitlab.com/vazhnov/devops_utils

set -o nounset
set -o errexit
shopt -s dotglob

for NetID in $(docker network ls -q); do
    echo -n "$NetID: "
    # How does it works? Read:
    # https://devops.stackexchange.com/questions/6241/how-do-i-use-docker-commands-format-option
    # https://container-solutions.com/docker-inspect-template-magic/
    docker network inspect $NetID --format='{{.Name}}, {{range .IPAM.Config}}{{.Subnet}}{{end}}'
done
