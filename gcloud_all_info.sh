#!/usr/bin/env bash
#
# Prints various information about your Google cloud projects.
#
# Prerequisites:
# Login to Google cloud:
# gcloud auth login
#
# Usage example:
# List all projects and resources:
# bash gcloud_all_info.sh
#
# List only resources in projects with names started with "pre":
# bash gcloud_all_info.sh pre
#
# Copyright:
# Distributed under CC0 1.0 license or newer.
#
# Source:
# https://gitlab.com/vazhnov/devops_utils

set -o nounset

gcloud projects list --format="table[box,title='All projects'](createTime.date('%Y-%m-%d'):sort=1,name,projectNumber,projectId,parent.id)"
for project in $(gcloud projects list --format="value(projectId)")
do
  if [[ $# == 0  || ( $# == 1 && $project == $1* ) ]]; then
    echo "ProjectId:  $project"
    gcloud dns managed-zones list --format="table[box,title='DNS managed-zones in project: $project'](name, dns_name, description)" --project=$project --quiet
    gcloud compute ssl-certificates list --format="table[box,title='compute ssl-certificates in project: $project'](name, creation_timestamp)" --project=$project --quiet
    gcloud compute networks subnets list --format="table[box,title='All subnets'](creationTimestamp.date('%Y-%m-%d'):sort=1, name, network, region, ipCidrRange)" --project=$project --quiet
    gcloud container clusters list --format="table[box,title='Container clusters in project: $project'](createTime.date('%Y-%m-%d'), name, network, zone, currentMasterVersion, currentNodeCount, endpoint, status)" --project=$project --quiet
    echo; echo "Compute instances in project: $project"
    gcloud compute instances list --project=$project --quiet
    echo; echo "target-ssl-proxies in project: $project"
    gcloud compute target-ssl-proxies list --project=$project
    gcloud compute target-pools list --format="table[box,title='target-pools in project: $project'](name,region,instances.map().basename().join(','))" --project=$project
    gcloud compute forwarding-rules list --format="table[box,title='Traffic forwarding rules to network load balancers in project: $project'](creationTimestamp.date('%Y-%m-%d'), region, IPAddress, IPProtocol, portRange, description, loadBalancingScheme, name, target)" --project=$project --quiet
    # TODO: make pretty table with all info about app services
    echo; echo "App services list in project: $project"
    gcloud app services list --format flattened --project=$project
  fi
done
