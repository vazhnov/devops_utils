# Some utils for DevOps

## Table of contents

* [gcloud\_all\_info.sh](#gcloud_all_info.sh)
* [docker\_network\_all\_subnets\_cidr.sh](#docker_network_all_subnets_cidr.sh)

## aws\_list\_all\_external\_addresses.sh

Prints all possible external IP-addresses of your AWS account:
* EC2 instances,
* Elastic balancers
* NAT gateway (those addresses for outgoing traffic)

Script: [aws\_list\_all\_external\_addresses.sh](https://gitlab.com/vazhnov/devops_utils/blob/master/aws_list_all_external_addresses.sh).

<a name="gcloud_all_info.sh"></a>
## gcloud\_all\_info.sh

Prints various information about your Google cloud projects and infrastructure.

Script: [gcloud\_all\_info.sh](https://gitlab.com/vazhnov/devops_utils/blob/master/gcloud_all_info.sh).

### Prerequisites

Login to Google cloud:

```shell
gcloud auth login
```

### Usage example

List all projects and resources:

```shell
bash gcloud_all_info.sh
```

List only resources in projects with names started with "pre":

```shell
bash gcloud_all_info.sh pre
```

<a name="docker_network_all_subnets_cidr.sh"></a>
## docker\_network\_all\_subnets\_cidr.sh

Prints all Docker network subnets ([CIDR](https://en.wikipedia.org/wiki/CIDR)s). Like `docker network ls`, but with CIDRs.

Script: [docker\_network\_all\_subnets\_cidr.sh](https://gitlab.com/vazhnov/devops_utils/blob/master/docker_network_all_subnets_cidr.sh).

How does it works? Here is all that you need to know:

* https://devops.stackexchange.com/questions/6241/how-do-i-use-docker-commands-format-option
* https://container-solutions.com/docker-inspect-template-magic/

### Prerequisites

Docker installed.

### Usage example

```shell
bash docker_network_all_subnets_cidr.sh
```

### Output example

```
8a968dd6e883: bridge, 172.17.0.0/16
bde82cf794a8: host,
ba51a24e07ac: none,
```

## Another utils

* [aws-tools](https://github.com/ezhuk/aws-tools) — Python scripts for AWS;
* [aws-queries.sh](https://github.com/InsightDataScience/pegasus/blob/master/aws-queries.sh) — good shell script;
* [bash-my-aws](https://github.com/bash-my-universe/bash-my-aws) — shell scripts and libraries with autocomplite;

## Copyright

Distributed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) license or newer.
