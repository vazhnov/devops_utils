#!/usr/bin/env bash
#
# Usage example:
# $ export AWS_PROFILE="my-dev-profile"
# $ bash aws_ec2_describe-instances_all_regions.sh --filter Name=instance-state-name,Values=running
#
# Copyright:
# Distributed under CC0 1.0 license or newer.
#
# Source:
# https://gitlab.com/vazhnov/devops_utils

set -o nounset
set -o errexit
shopt -s dotglob

for region in $(aws ec2 describe-regions --region=us-east-1 --query 'Regions[].RegionName' --output=text); do
	echo "Region: $region"
	# shellcheck disable=SC2016
	aws ec2 --region="$region" describe-instances "$@" --query 'Reservations[].Instances[].[Tags[?Key==`Name`]|[0].Value, State.Name, InstanceId, Placement.AvailabilityZone, InstanceType, PrivateIpAddress, PublicIpAddress, Platform, LaunchTime] | sort_by(@, &[8])' --output table
done
