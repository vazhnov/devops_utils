#!/usr/bin/env bash

# For your filter, get information about termination protection and enable termination protection.
#
# Usage example:
# Get info and enable termination protection for whole VPC (all instances):
# export AWS_DEFAULT_REGION="us-east-1"
# export AWS_PROFILE=0123456789
# bash aws_ec2_termination_protection_enable.sh --filter Name=vpc-id,Values=vpc-0123a3278eb372a60
#
# Copyright:
# Distributed under CC0 1.0 license or newer.
#
# Source:
# https://gitlab.com/vazhnov/devops_utils

set -o nounset
set -o errexit
shopt -s dotglob

for instance in $(aws ec2 describe-instances $@ --query 'Reservations[].Instances[].InstanceId' --output text); do
	echo "Instance: $instance"
	aws ec2 describe-instance-attribute --instance-id "$instance" --attribute disableApiTermination --query "['Was: termination protection: ', to_string(DisableApiTermination.Value)]" --output text
	aws ec2 modify-instance-attribute --instance-id $instance --disable-api-termination && { echo "Successfully set: disable-api-termination"; } || { echo "Can't set: disable-api-termination"; }
done
